# Ansible Role: Aegir-Minion

Configuration for a [remote web server](https://docs.aegirproject.org/usage/servers/remote-servers/) under [Aegir 3](https://aegirproject.org).

Ubuntu Focal (20.04) is the currently supported OS version. Debian (or any OS that
supports apt) should work, too, but YMMV.

## Prerequisites

* [consensus.php-versions](https://github.com/consensus-enterprises/ansible-role-php-versions)
* [consensus.php](https://github.com/consensus-enterprises/ansible-role-php)
* [consensus.mysql](https://github.com/consensus-enterprises/ansible-role-mysql)
* One of [consensus.nginx](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-nginx) or [consensus.apache](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-apache)

## Example Playbook

```
---
- name: "Aegir minion server install."
  hosts: all 

  vars:
    mysql_root_password: {{ mysql_root_password }}  
    nginx_manage_config: False
    nginx_remove_default_vhost: True
    php_cli_memory_limit: "4096M"
    php_enable_php_fpm: True
    php_enable_webserver: False
    php_set_cli_memory_limit: True
    php_version: "8.1"
    aegir_db_service_type: mysql
    aegir_db_pass: {{ mysql_root_password }}
    aegir_http_service_type: nginx
    aegir_ssh_pub_key: {{ aegir_ssh_pub_key }}

  roles:
    - consensus.php-versions
    - consensus.aegir-minion
```

## License

[GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)

## Author Information

[Christopher Gervais](https://consensus.enterprises/team/christopher/), [Derek Laventure](https://consensus.enterprises/team/derek/), [Dan Friedman](https://consensus.enterprises/team/dan/).
